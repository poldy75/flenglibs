-initialization(main).

main :-
    i18n:lc_all(LcAll),
    i18n:setlocale(LcAll, '', _) &
    directory('.', Items),
    main_loop(Items).

-mode(main_loop(?)).
main_loop([Name | Tail]) :-
    posix:mkstat(StatBuf),
    list_to_string(Name, NameStr),
    posix:stat(NameStr, StatBuf, Ok),
    when(Ok, write_entry(NameStr, StatBuf, Tail)).
main_loop([]) :- true.

-mode(write_entry(?, ?, ?)).
write_entry(FileName, StatBuf, Tail) :- string(FileName) |
    posix:stat_st_nlink(StatBuf, NLink),
    posix:stat_st_uid(StatBuf, Uid),
    posix:getpwuid(Uid, Passwd),
    posix:passwd_pw_name(Passwd, UserName),
    posix:stat_st_gid(StatBuf, Gid),
    posix:getgrgid(Gid, Group),
    posix:group_gr_name(Group, GroupName),
    posix:stat_st_size(StatBuf, Size),
    posix:stat_st_mtime(StatBuf, MTime),
    i18n:d_t_fmt(DTFmt),
    i18n:nl_langinfo(DTFmt, DTFmtStr),
    cstd:localtime(MTime, TimePtr),
    cstd:strftime(DTFmtStr, TimePtr, MTimeStr),
    posix:stat_st_mode(StatBuf, Mode),
    mode_to_string(Mode, ModeStr),
    fmt:format(stdout, '~s  ~d  ~s  ~s  ~d  ~s  ~s~n', [ModeStr, NLink, UserName, GroupName, Size, MTimeStr, FileName], Printed),
    when(Printed, cstd:free(StatBuf, _)),
    main_loop(Tail).

-mode(get_modes(^)).
get_modes(Modes) :- Modes := ['---', '--x', '-w-', '-wx', 'r--', 'r-x', 'rw-', 'rwx'].

-mode(mode_to_string(?, ^)).
mode_to_string(Mode, Str) :- integer(Mode) |
    filetype_to_char(Mode, FileTypeCh),
    get_modes(Modes),
    UserPerm is ((Mode >> 6) /\ 7) + 1,
    list:nth(UserPerm, Modes, UserPermStr),
    GroupPerm is ((Mode >> 3) /\ 7) + 1,
    list:nth(GroupPerm, Modes, GroupPermStr),
    OtherPerm is (Mode /\ 7) + 1,
    list:nth(OtherPerm, Modes, OtherPermStr),
    fmt:format_chars('~c~s~s~s', [FileTypeCh, UserPermStr, GroupPermStr, OtherPermStr], Str).

-mode(filetype_to_char(?, ^)).
filetype_to_char(Mode, FileTypeCh) :- integer(Mode) |
    posix:s_isdir(Mode, IsDir),
    posix:s_isfifo(Mode, IsFifo),
    posix:s_islnk(Mode, IsLnk),
    posix:s_isreg(Mode, IsReg),
    posix:s_issock(Mode, IsSock),
    posix:s_isblk(Mode, IsBlk),
    posix:s_ischr(Mode, IsChr),
    filetype_char(IsDir, IsFifo, IsLnk, IsReg, IsSock, IsBlk, IsChr, FileTypeCh).

-mode(filetype_char(?, ?, ?, ?, ?, ?, ?, ^)).
filetype_char(IsDir, _, _, _, _, _, _, Ch) :- IsDir == true | Ch := 0'd.
filetype_char(_, IsFifo, _, _, _, _, _, Ch) :- IsFifo == true | Ch := 0'p.
filetype_char(_, _, IsLnk, _, _, _, _, Ch) :- IsLnk == true | Ch := 0'l.
filetype_char(_, _, _, IsReg, _, _, _, Ch) :- IsReg == true | Ch := 0'-.
filetype_char(_, _, _, _, IsSock, _, _, Ch) :- IsSock == true | Ch := 0's.
filetype_char(_, _, _, _, _, IsBlk, _, Ch) :- IsBlk == true | Ch := 0'b.
filetype_char(_, _, _, _, _, _, IsChr, Ch) :- IsChr == true | Ch := 0'c.
