%= cdb interface
%
%-
% cdb:init(FILENAME?, PORT^)
%       Open database with the given filename and assign a port to PORT
%       receiving any of the following access messages:
%
%       close
%       exists(KEY?, VAL^)
%       exists(KEY?, SKIP?, VAL^)
%       fetch(KEY?, VAL^, TAIL?)
%       fetch(KEY?, SKIP?, VAL^, TAIL?)
%
% cdb:provide(FILENAME?, ARGS?)
%       Fetches database entry for ARGS in database given by FILENAME.
%       FILENAME must be a string.

-module(cdb).

init(Name, DB) :-
    open_port(DB, S),
    open_file(Name, r, File),
    db_init(File, DBref),
    loop(S, DBref, File).

db_init(!File, DBref) :- foreign_call(fl_cdb_init(File, DBref)).

loop([], DBref, File) :- 
    foreign_call(fl_cdb_free(DBref, Ok)),
    when(Ok, close_file(File, _)).
loop([close|_], DBref, File) :- loop([], DBref, File).
loop([exists(Key, Val)|S], DBref, File) :-
    loop([exists(Key, 0, Val)|S], DBref, File).
loop([exists(Key, Skip, Val)|S], DBref, File) :-
    db_find(Key, DBref, Skip, Val),
    when(Val, loop(S, DBref, File)).
loop([fetch(Key, Val, Tail)|S], DBref, File) :-
    loop([fetch(Key, 0, Val, Tail)|S], DBref, File).
loop([fetch(Key, Skip, Val, Tail)|S], DBref, File) :-
    db_find(Key, DBref, Skip, Found),
    db_read(Found, Key, DBref, Val, Tail),
    when(Val, loop(S, DBref, File)).
loop([M|_], _, _) :-
    error('cdb: invalid message'(M)).

db_find(Key, !DBref, !Skip, Found) :-
    deref(Key, Ok),
    when(Ok, foreign_call(fl_cdb_find(DBref, Key, Found1))),
    db_findnext(Found1, DBref, Skip, Found).

db_findnext(true, _, 0, Found) :- Found = true.
db_findnext(true, !DBref, N, Found) :- 
    N > 0 | 
    N2 is N - 1,
    foreign_call(fl_cdb_find_next(DBref, Found1)),
    db_findnext(Found1, DBref, N2, Found).
db_findnext(false, _, _, Found) :- Found = false.

db_read(true, _, !DBref, Val, Tail) :- 
    foreign_call(fl_cdb_read(DBref, Val, Tail)).
db_read(false, Key, _, _, _) :-
    error('cdb: key not found'(Key)).

provide(DBname, Args) :- 
    foreign_call(fl_cdb_provide(DBname, Args)).
