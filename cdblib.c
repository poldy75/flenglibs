/* interface to djb's cdb database */

#include <fleng.h>
#include <fleng-util.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "cdb.h"

void fl_cdb_init_2(FL_TCB *tcb, FL_VAL fd, FL_VAL var)
{
    CHECK_INT(fd);
    struct cdb *db = malloc(sizeof(struct cdb));
    cdb_init(db, INT(fd));
    fl_assign(tcb, MARK(db), var);
}

void fl_cdb_free_2(FL_TCB *tcb, FL_VAL db, FL_VAL done)
{
    CHECK_INT(db);
    struct cdb *p = (struct cdb *)UNMARK(db);
    cdb_free(p);
    fl_assign(tcb, fl_nil, done);
}

void fl_cdb_read_3(FL_TCB *tcb, FL_VAL db, FL_VAL result, FL_VAL tail)
{
    CHECK_INT(db);
    struct cdb *p = (struct cdb *)UNMARK(db);
    int len = cdb_datalen(p);
    char *buf = malloc(len);
    if(cdb_read(p, buf, len, cdb_datapos(p)) != 0)
        fl_rt_error(tcb, db, FL_IO_ERROR);
    FL_VAL lst = mkcharlist(tcb, buf, len, fl_nil);
    free(buf);
    fl_assign(tcb, lst, result);
}

static char *findk;
static int findlen;

void fl_cdb_find_3(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL result)
{
    CHECK_INT(db);
    struct cdb *p = (struct cdb *)UNMARK(db);
    char *buf = stringify(tcb, key, &findlen);
    if(findk != NULL) free(findk);
    findk = strndup(buf, findlen);
    int n = cdb_find(p, findk, findlen);
    if(n < 0) fl_rt_error(tcb, db, FL_IO_ERROR);
    if(n == 0) fl_assign(tcb, fl_false, result);
    else fl_assign(tcb, fl_true, result);
}

void fl_cdb_find_next_2(FL_TCB *tcb, FL_VAL db, FL_VAL result)
{
    CHECK_INT(db);
    struct cdb *p = (struct cdb *)UNMARK(db);
    assert(findk != NULL);
    int n = cdb_findnext(p, findk, findlen);
    if(n < 0) fl_rt_error(tcb, db, FL_IO_ERROR);
    if(n == 0) fl_assign(tcb, fl_false, result);
    else fl_assign(tcb, fl_true, result);
}

void fl_cdb_provide_2(FL_TCB *tcb, FL_VAL dbname, FL_VAL args)
{
    CHECK_STRING(dbname);
    FL_VAL db = fl_get_global(tcb, dbname, fl_nil);
    if(db == fl_nil) {
        char *s = stringify(tcb, dbname, NULL);
        int fd = open(s, O_RDONLY);
        if(fd == -1) fl_rt_error(tcb, dbname, FL_IO_ERROR);
        struct cdb *pdb = malloc(sizeof(struct cdb));
        cdb_init(pdb, fd);
        close(fd);
        fl_set_global(tcb, dbname, db = MARK(pdb));
    }
    struct cdb *pdb = (struct cdb *)UNMARK(db);
    LIST(args, key, more);
    FL_VAL result = deref(CAR(more));
    int klen;
    char *k = stringify(tcb, key, &klen);
    int n = cdb_find(pdb, k, klen);    
    if(n < 0) fl_rt_error(tcb, dbname, FL_IO_ERROR);
    if(n == 0) fl_assign(tcb, fl_false, result);
    else {
        int len = cdb_datalen(pdb);
        char *buf = malloc(len);
        if(cdb_read(pdb, buf, len, cdb_datapos(pdb)) != 0)
            fl_rt_error(tcb, db, FL_IO_ERROR);
        FL_VAL lst = mkcharlist(tcb, buf, len, fl_nil);
        free(buf);
        fl_assign(tcb, lst, result);
    }
}
