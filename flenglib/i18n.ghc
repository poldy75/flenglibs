%= "i18n" module - Internationalization
%
% This is intended to support I/O in different human languages.  Coverage
% is currently incomplete, but hopefully this is a good start.  All below
% predicates and messages should be understood as returning the correct
% result for the current locale.
%
% Some fundamental design assumptions are that FGHC strings are
% UTF-8 encoded, charlists contain code points, and I/O will be in the UTF-8 encoding.
%
% See the 'ucs' module for further useful predicates.
%
%-
% 'i18n':create_cat(NAME?, RESULT^)
%       Opens message catalog NAME.  RESULT is
%       a stream of request tuples, described below.
%       Corresponds to the 'catopen' library function.
%_
%    Valid request tupes are the following:
%_
%       gets(SETID?, MSGID?, STR?, RESULT^)
%        Gets message (SETID, MSGID) from the catalog, with STR as a
%        fallback in case of errors.  Corresponds to the 'catgets' library
%        function.
%
% 'i18n':setlocale(CATEGORY?, LOCALE?, RESULT^)
%       Sets the process locale to LOCALE.  LOCALE should usually be the empty
%       string ("") to take the correct value from the user's environment.
%       CATEGORY should be the value from 'lc_all'.
%       The locale that was set is returned to RESULT.  Corresponds to the
%       'setlocale' library function.
%
% 'i18n':strcoll(S1?, S2?, RESULT^)
%       Collate two strings, essentially a locale-aware version of
%       'strcmp'.  RESULT is the usual <0, 0 or >0 value for lexicographic
%       comparison.  Corresponds to the 'strcoll' library function.
%
% 'i18n':nl_langinfo(ITEM?, INFO^)
%       Get locale information.
%       See the nl_langinfo(3) man page for details.
%
% 'i18n':lc_all(LC_ALL^)
%       Gets the "all" locale category for use with 'setlocale'.
%       Corresponds to the 'LC_ALL' macro.
%
% 'i18n':d_t_fmt(DTFmt^)
%       Gets the local date-time format for use with 'nl_langinfo'.
%       Corresponds to the 'D_T_FMT' macro.

-foreign('#include <string.h>').
-foreign('#include <locale.h>').
-foreign('#include <nl_types.h>').
-foreign('#include <langinfo.h>').
-foreign([setlocale(integer, string, -string),
          catopen(string, integer, -pointer),
          catgets(pointer, integer, integer, string, -string),
          catclose(pointer, -integer),
          strcoll(string, string, -integer),
	  nl_langinfo(integer, -string)]).
-module(i18n).

-mode(create_cat(?, ^)).
create_cat(Name, P) :-
    open_port(P, S),
    nl_cat_locale(NlCatLocale),
    catopen(Name, NlCatLocale, R),
    wait(S, R).

-mode(wait(?, ?)).
wait([gets(Set, Msg, Default, Result)|S], R) :- integer(Set), integer(Msg), string(Default) |
    catgets(R, Set, Msg, Default, Result),
    when(Result, wait(S, R)).
wait([], R) :-
    catclose(R, _).

-foreign(const(lc_all = 'LC_ALL':integer)).
-foreign(const(nl_cat_locale = 'NL_CAT_LOCALE':integer)).
-foreign(const(d_t_fmt = 'D_T_FMT':integer)).
