#include "fleng.h"
#include "fleng-util.h"
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>

struct timespec *mktimespecpair(time_t sec1, time_t sec2)
{
    struct timespec *result = malloc(sizeof(struct timespec[2]));
    result->tv_sec = sec1;
    result->tv_nsec = 0;
    (result + 1)->tv_sec = sec2;
    (result + 1)->tv_nsec = 0;
    return result;
}

void posix_getgroups_1(FL_TCB *tcb, FL_VAL result)
{
    FL_VAL lst, tl = fl_nil;
    gid_t gidset[NGROUPS_MAX];
    int ngroups = getgroups(NGROUPS_MAX, gidset);
    int i;
    if (ngroups < 0) fl_assign(tcb, fl_false, result);
    else {
	for (i = 0; i < ngroups; i++) {
	    FL_VAL group = fl_alloc_cell(tcb, MKINT(gidset[i]), fl_nil);
            ((FL_CELL *)group)->tag = FL_LIST_TAG;
	    if (tl != fl_nil) ((FL_CELL *)tl)->cdr = addref(group);
	    else lst = group;
	    tl = group;
	}
	if (tl != fl_nil) ((FL_CELL *)tl)->cdr = addref(fl_nil);
	else lst = fl_nil;
	fl_assign(tcb, lst, result);
    }
}
