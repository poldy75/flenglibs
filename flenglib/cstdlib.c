#include "fleng.h"
#include "fleng-util.h"
#include <time.h>

#define INIT_CBUF 10000

void cstd_strftime_3(FL_TCB *tcb, FL_VAL format, FL_VAL timeptr, FL_VAL result)
{
    CHECK_INT(timeptr);
    struct tm *tp = (struct tm *)POINTER(timeptr);
    char *buf = init_cbuf(tcb);
    char *fmt = stringify(tcb, format, NULL);
    int len = strftime(buf, INIT_CBUF, fmt, tp);
    if(len == 0) fl_assign(tcb, fl_false, result);
    else {
	FL_VAL lst = mkcharlist(tcb, buf, len, fl_nil);
	fl_assign(tcb, lst, result);
    }
}
