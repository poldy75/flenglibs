/* interface to curses */

#include <fleng.h>
#include <fleng-util.h>
#include <curses.h>
#include <string.h>

void fl_curses_getnstr_3(FL_TCB *tcb, FL_VAL n, FL_VAL result, FL_VAL tail)
{
    CHECK_INT(n);
    int len = INT(n);
    char str[len + 1];
    int status = getnstr(str, len);
    if (status == ERR) fl_assign(tcb, fl_false, result);
    else {
        FL_VAL lst = mkcharlist(tcb, str, strlen(str), tail);
        fl_assign(tcb, lst, result);
    }
}
