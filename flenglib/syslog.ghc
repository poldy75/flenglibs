%= "syslog" module - logging using syslog
%
% This module provides a thin interface for the standard UNIX logging facility.
% Use "syslog:syslog" to log messages.
%
% The syslog interface uses several constants defined in header files
% whose values were not standardised.  The syslog man pages should be
% referred to for their meaning.  Predicates are also provided to get these
% values from FGHC code.
%
%-
% 'syslog':openlog(IDENT?, LOGOPTS?, FACILITY?, RESULT^)
%       Configure logging for the current process.  IDENT should be the
%       process name, LOGOPTS and FACILITY can be got from the subsection
%       below and the man pages.  RESULT is currently unused.  Corresponds
%       to the 'openlog' library function.
%
% 'syslog':syslog(PRIORITY?, MESSAGE?, RESULT^)
%       Log a message.  PRIORITY can be chosen from the subsection below.
%       RESULT is currently unused.  Corresponds to the 'syslog' library
%       function.
%
% 'syslog':closelog(RESULT^)
%       Close the connection to the system logger.  RESULT is currently
%       unused.  Corresponds to the 'closelog' library function.
%
% 'syslog':log_pid(LOGPID^)
%       Flag to include process ID in log messages.  Corresponds to the
%       'LOG_PID' preprocessor macro.
%
% 'syslog':log_local0(LOGLOCAL0^)
%       The "Local 0" facility.  Corresponds to the 'LOG_LOCAL0'
%       preprocessor macro.
%
% 'syslog':log_alert(LOGALERT^)
%       The "Alert" message priority.  Corresponds to the 'LOG_ALERT'
%       preprocessor macro.
%
% 'syslog':log_crit(LOGCRIT^)
%       The "Critical" message priority.  Corresponds to the 'LOG_CRIT'
%       preprocessor macro.
%
% 'syslog':log_err(LOGERR^)
%       The "Error" message priority.  Corresponds to the 'LOG_ERR'
%       preprocessor macro.
%
% 'syslog':log_warning(LOGwarning^)
%       The "Warning" message priority.  Corresponds to the 'LOG_WARNING'
%       preprocessor macro.
%
% 'syslog':log_notice(LOGNOTICE^)
%       The "Notice" message priority.  Corresponds to the 'LOG_NOTICE'
%       preprocessor macro.
%
% 'syslog':log_info(LOGINFO^)
%       The "Information" message priority.  Corresponds to the 'LOG_INFO'
%       preprocessor macro.
%
% 'syslog':log_debug(LOGDEBUG^)
%       The "Debug" message priority.  Corresponds to the 'LOG_DEBUG'
%       preprocessor macro.

-foreign('#include <syslog.h>').
-foreign([openlog(charlist, integer, integer, -void),
          syslog(integer, charlist, -void),
          closelog(-void)]).
-module(syslog).
-foreign(const(log_pid = 'LOG_PID':integer)).
-foreign(const(log_local0 = 'LOG_LOCAL0':integer)).
-foreign(const(log_alert = 'LOG_ALERT':integer)).
-foreign(const(log_crit = 'LOG_CRIT':integer)).
-foreign(const(log_err = 'LOG_ERR':integer)).
-foreign(const(log_warning = 'LOG_WARNING':integer)).
-foreign(const(log_notice = 'LOG_NOTICE':integer)).
-foreign(const(log_info = 'LOG_INFO':integer)).
-foreign(const(log_debug = 'LOG_DEBUG':integer)).

