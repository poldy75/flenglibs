%= "posix" module - wrapper for UNIX programming interfaces
%
% Credit to Scheme's SRFI-170, which I used as
% a source of what subset of functionality is most useful.
% Note that quite a few syscalls are already wrapped in the
% "lib" module, see its documentation for further details.
%
% In particular, the following are in lib: readlink, rmdir, getcwd, chdir, setenv, isatty.
% Lib:directory is the same as opendir, readdir & closedir.
% Path:normalize is the same as realpath.
% And getpid is a builtin.
%
% Documentation is quite light, because everything relevant
% should be in your operating system's man pages already.
%
%-
% 'posix':open(PATH?, OFLAG?, MODE?, FD^)
%       Open or create a file for reading or writing.
%       Corresponds to the 'open' syscall.
%
% 'posix':mkdir(PATH?, MODE?, RESULT^)
%       Make a directory file.
%       Corresponds to the 'mkdir' syscall.
%
% 'posix':mkfifo(PATH?, MODE?, RESULT^)
%       Make a FIFO file.
%       Corresponds to the 'mkfifo' syscall.
%
% 'posix':link(PATH1?, PATH2?, RESULT^)
%       Make hard link to a file.
%       Corresponds to the 'link' syscall.
%
% 'posix':symlink(PATH1?, PATH2?, RESULT^)
%       Make symbolic link to a file.
%       Corresponds to the 'symlink' syscall.
%
% 'posix':utimensat(FD?, PATH?, ATIMESEC?, MTIMESEC?, FLAG?, RESULT^)
%       Set file access and modification times.
%       Implemented using the 'utimensat' syscall.
%
% 'posix':truncate(PATH?, LENGTH?, RESULT^)
%       Truncate or extend a file to a specified length.
%       Corresponds to the 'truncate' syscall.
%
% 'posix':mkstat(SB^)
%       Allocate a buffer for use with the 'stat' predicate.
%       NB: this must be freed manually when no longer needed.
%
% 'posix':stat(PATH?, SB?, RESULT^)
%       Get file status.
%       Corresponds to the 'stat' syscall.
%
%+ posix:s_isdir s_isfifo s_islnk s_isreg s_issock s_isblk s_ischr
% 'posix':s_isdir(MODE?, FLAG^)
% 'posix':s_isfifo(MODE?, FLAG^)
% 'posix':s_islnk(MODE?, FLAG^)
% 'posix':s_isreg(MODE?, FLAG^)
% 'posix':s_issock(MODE?, FLAG^)
% 'posix':s_isblk(MODE?, FLAG^)
% 'posix':s_ischr(MODE?, FLAG^)
%       Assign the string "true" or "false" to FLAG,
%       depending on whether the file is of the given type.
%       See the stat(2) man page for details.
%
% 'posix':chmod(PATH?, MODE?, RESULT^)
%       Change mode of file.
%       Corresponds to the 'chmod' syscall.
%
%+ posix:statvfs_avail fstatvfs_avail
% 'posix':statvfs_avail(PATH?, AVAIL^)
% 'posix':fstatvfs_avail(FD?, AVAIL^)
%       Returns the amount of free space in bytes on the same volume as the file PATH (for 'statvfs_avail') or
%       the file open on FD (for 'fstatvfs_avail').
%       Implemented using the 'statvfs' & 'fstatvfs' syscalls.
%
% 'posix':umask(NUMASK?, RESULT^)
%       Set file creation mode mask.
%       Corresponds to the 'umask' syscall.
%
% 'posix':nice(INCR?, RESULT^)
%       Change process scheduling priority.
%       Corresponds tot he 'nice' library function.
%
%+ posix:getuid getgid geteuid getegid
% 'posix':getuid(UID^)
% 'posix':getgid(GID^)
% 'posix':geteuid(UID^)
% 'posix':getegid(GID^)
%       Get user or group process identification.
%       Correspond to the identically-named syscalls.
%
% 'posix':getgroups(GROUPS^)
%       Get group access list.
%       Implemented using the 'getgroups' syscall.
%
%+ posix:getpwuid getpwnam getgrgid getgrnam
% 'posix':getpwuid(UID?, PASSWD^)
% 'posix':getpwnam(LOGIN?, PASSWD^)
% 'posix':getgrgid(GID?, GROUP^)
% 'posix':getgrnam(NAME?, GROUP^)
%       Password & group database operations.
%       Correspond to the identically-named library functions.
%
% 'posix':mktimespec(TIMESPEC^)
%       Allocate a buffer for use with the 'clock_gettime' predicate.
%       NB: this must be freed manually when no longer needed.
%
% 'posix':clock_gettime(CLOCK?, TIMESPEC?, RESULT^)
%       Get the time.
%       Corresponds to the 'clock_gettime' syscall.
%
% 'posix':unsetenv(NAME?, RESULT^)
%       Delete the environment variable NAME.
%       Corresponds to the 'unsetenv' library function.
%
%+ posix:o_rdonly o_wronly o_rdwr o_append o_creat o_excl o_nofollow o_trunc
% 'posix':o_rdonly(OFLAG^)
% 'posix':o_wronly(OFLAG^)
% 'posix':o_rdwr(OFLAG^)
% 'posix':o_append(OFLAG^)
% 'posix':o_creat(OFLAG^)
% 'posix':o_excl(OFLAG^)
% 'posix':o_nofollow(OFLAG^)
% 'posix':o_trunc(OFLAG^)
%       OFLAG bitmask argument to the 'open' syscall.
%       See the open(2) man page for details.
%
%+ posix:clock_realtime clock_monotonic
% 'posix':clock_realtime(CLOCK^)
% 'posix':clock_monotonic(CLOCK^)
%       CLOCK argument to the 'clock_gettime' syscall.
%       See the clock_gettime(2) man page for details.

-foreign('#include <fcntl.h>').
-foreign('#include <sys/types.h>').
-foreign('#include <pwd.h>').
-foreign('#include <unistd.h>').
-foreign('#include <sys/stat.h>').
-foreign('#include <string.h>').
-foreign('#include <sys/statvfs.h>').
-foreign('#include <grp.h>').
-foreign('struct stat *mkstat(void) {return malloc(sizeof(struct stat));}').
-foreign('struct timespec *mktimespecpair(time_t sec1, time_t sec2);').
-foreign('struct timespec *mktimespec(void) {return malloc(sizeof(struct timespec));}').
-foreign(struct(timespec = 'struct timespec'(-tv_sec:long, -tv_nsec:long))).
-foreign(struct(stat = 'struct stat'(-st_dev:integer, -st_ino:long, -st_mode:integer, -st_nlink:integer, -st_uid:integer, -st_gid:integer, -st_rdev:integer, -st_atime:integer, -st_mtime:long, -st_ctime:long, -st_size:long, -st_blocks:long, -st_blksize:integer))).
-foreign(struct(statvfs = 'struct statvfs'(-f_bsize:long, -f_bavail:long))).
-foreign('static struct statvfs *mkstatvfs(void) {return malloc(sizeof(struct statvfs));}').
-foreign(struct(passwd = 'struct passwd'(-pw_name:string, -pw_uid:integer, -pw_gid:integer, -pw_dir:string, -pw_shell:string, -pw_gecos:string))).
-foreign(struct(group = 'struct group'(-gr_name:string, -gr_gid:integer))).
-foreign([open(string, integer, integer, -integer),
          mkdir(string, integer, -integer),
          mkfifo(string, integer, -integer),
          link(string, string, -integer),
          symlink(string, string, -integer),
          % readlink(string, pointer, integer, -integer),
          rename(string, string, -integer),
          % rmdir(string, -integer),
          chown(string, integer, integer, -integer),

	  mktimespecpair(long, long, -pointer('struct timespec')),
          utimensat_c = utimensat(integer, string, pointer('struct timespec'), integer, -integer),

          truncate(string, long, -integer),

	  mkstat(-pointer('struct stat')),
	  stat(string, pointer('struct stat'), -integer),
	  s_isdir_c = 'S_ISDIR'(integer, -integer),
	  s_isfifo_c = 'S_ISFIFO'(integer, -integer),
	  s_islnk_c = 'S_ISLNK'(integer, -integer),
	  s_isreg_c = 'S_ISREG'(integer, -integer),
	  s_issock_c = 'S_ISSOCK'(integer, -integer),
	  s_isblk_c = 'S_ISBLK'(integer, -integer),
	  s_ischr_c = 'S_ISCHR'(integer, -integer),

	  chmod(string, integer, -integer),
	  % opendir(string, -pointer),
	  % readdir(pointer, -dirent),
	  % closedir(pointer, -integer),
	  % realpath(string, pointer, -pointer),
	  mkstatvfs(-pointer('struct statvfs')),
	  statvfs(string, pointer('struct statvfs'), -integer),
	  fstatvfs(integer, pointer('struct statvfs'), -integer),
          umask(integer, -integer),
	  nice(integer, -integer),
	  getuid(-integer),
	  getgid(-integer),
	  geteuid(-integer),
	  getegid(-integer),
          getpwuid(integer, -pointer('struct passwd')),
	  getpwnam(string, -pointer('struct passwd')),
          getgrgid(integer, -pointer('struct group')),
	  getgrnam(string, -pointer('struct group')),
	  mktimespec(-pointer('struct timespec')),
	  clock_gettime(integer, pointer('struct timespec'), -integer),
          % setenv(string, string, -integer),
          unsetenv(string, -integer)]).
          % isatty(integer, -integer),
-module(posix).

-mode(utimensat(?, ?, ?, ?, ?, ^)).
utimensat(Fd, Path, ATimeSec, MTimeSec, Flag, Result) :- integer(Fd), string(Path), integer(ATimeSec), integer(MTimeSec), integer(Flag) |
    mktimespecpair(ATimeSec, MTimeSec, Times),
    utimensat_c(Fd, Path, Times, Flag, Result),
    when(Result, cstd:free(Times, _)).

-mode(statvfs_avail(?, ^)).
statvfs_avail(Path, Avail) :- string(Path) |
    mkstatvfs(Buf),
    statvfs(Path, Buf, Result),
    when(Result, calc_avail(Buf, Avail)),
    when(Avail, cstd:free(Buf, _)).

-mode(fstatvfs_avail(?, ^)).
fstatvfs_avail(Fd, Avail) :- integer(Fd) |
    mkstatvfs(Buf),
    fstatvfs(Fd, Buf, Result),
    when(Result, calc_avail(Buf, Avail)),
    when(Avail, cstd:free(Buf, _)).

-mode(calc_avail(?, ^)).
calc_avail(Buf, Avail) :-
    statvfs_f_bsize(Buf, BSize),
    statvfs_f_bavail(Buf, BAvail),
    Avail := BSize * BAvail.

-mode(getgroups(^)).
getgroups(All) :- foreign_call(posix_getgroups(All)).

-mode(s_isdir(?, ^)).
s_isdir(Mode, Flag) :- integer(Mode) |
    s_isdir_c(Mode, RetCode),
    int_to_flag(RetCode, Flag).

-mode(s_isfifo(?, ^)).
s_isfifo(Mode, Flag) :- integer(Mode) |
    s_isfifo_c(Mode, RetCode),
    int_to_flag(RetCode, Flag).

-mode(s_islnk(?, ^)).
s_islnk(Mode, Flag) :- integer(Mode) |
    s_islnk_c(Mode, RetCode),
    int_to_flag(RetCode, Flag).

-mode(s_isreg(?, ^)).
s_isreg(Mode, Flag) :- integer(Mode) |
    s_isreg_c(Mode, RetCode),
    int_to_flag(RetCode, Flag).

-mode(s_issock(?, ^)).
s_issock(Mode, Flag) :- integer(Mode) |
    s_issock_c(Mode, RetCode),
    int_to_flag(RetCode, Flag).

-mode(s_isblk(?, ^)).
s_isblk(Mode, Flag) :- integer(Mode) |
    s_isblk_c(Mode, RetCode),
    int_to_flag(RetCode, Flag).

-mode(s_ischr(?, ^)).
s_ischr(Mode, Flag) :- integer(Mode) |
    s_ischr_c(Mode, RetCode),
    int_to_flag(RetCode, Flag).

-mode(int_to_flag(?, ^)).
int_to_flag(Num, Flag) :- Num =:= 0 | Flag := false.
int_to_flag(Num, Flag) :- Num \=:= 0 | Flag := true.

-foreign(const(o_rdonly = 'O_RDONLY':integer)).
-foreign(const(o_wronly = 'O_WRONLY':integer)).
-foreign(const(o_rdwr = 'O_RDWR':integer)).
-foreign(const(o_append = 'O_APPEND':integer)).
-foreign(const(o_creat = 'O_CREAT':integer)).
-foreign(const(o_excl = 'O_EXCL':integer)).
-foreign(const(o_nofollow = 'O_NOFOLLOW':integer)).
-foreign(const(o_trunc = 'O_TRUNC':integer)).
-foreign(const(clock_realtime = 'CLOCK_REALTIME':integer)).
-foreign(const(clock_monotonic = 'CLOCK_MONOTONIC':integer)).
